@ECHO OFF

SETLOCAL EnableDelayedExpansion

::------
:: USAGE
::------

GOTO START

:USAGE
   ECHO.
   ECHO USAGE:
   ECHO.
   ECHO   create-plot-files.bat -n -i -1 -2 -s
   ECHO     -n : Number of plotfiles (default: 10)
   ECHO     -1 : Path of the first source disk (e.g. C:\Mount\001\plots)
   ECHO     -2 : Path of the second source disk (e.g. C:\Mount\002\plots)
   ECHO     -d : Path of the destination disk (e.g. C:\Mount\003\plots)
   ECHO     -s : Specify the size of the disk in TB (e.g. 8)

   ECHO.
   ECHO EXAMPLE:
   ECHO.
   ECHO   create-plot-files.bat
   ECHO.

   EXIT /B 1

:START
   IF [%~1]==[] GOTO USAGE
   IF [%~2]==[] GOTO USAGE
::   IF [%~3]==[] GOTO USAGE

::----------------
:: INPUT ARGUMENTS
::----------------

SET COUNTER=0
:: SET NUM_OF_FILES=%1 (-n)
:: SET NUMERIC_ID=%5 (-i)
:: SET SRC_PATH_1=%4 (-1)
:: SET SRC_PATH_2=%5 (-2)
:: SET DST_PATH=%6 (-d)
:: SET DISK_SIZE=%5 (-s)
:: -t 2017 -i 8833445048739881147 -1 C:\_Array2\Disk005\plots_staging -2 C:\_Array2\Disk006\plots_staging -d C:\_Array2\Disk017\plots -n 3049312
:: -n < number_of_plotfiles> -i <numeric_id> -d <destination_disk>

:: IF "%1"=="" ( SET "NUM_OF_FILES=10" ) ELSE ( SET "NUM_OF_FILES=%1" )
:: IF "%2"=="" ( SET "DatabaseName=MyDatabase" ) ELSE ( SET "DatabaseName=%2" )

SET DISKNUM=%~1
SET NUMERIC_ID=%~2
SET SRC_PATH_1=%~3
SET SRC_PATH_2=%~4
SET DST_PATH=%~5
SET NONCES=%~6

SET DISK_SIZE=8
SET NUM_OF_FILES=10
SET /A FILE_SIZE=(!NONCES!*256)/(1024*1024)

SET START_NONCE=0
SET /A END_NONCE=!NONCES! * (!NUM_OF_FILES! - 1) + !START_NONCE!

:: SET /A NONCES = 381164 * !DISK_SIZE!
:: SET /A END_NONCE = !NONCES! * 9 + !START_NONCE!

:: FOR /L %%i IN (!START_NONCE!,!NONCES!,!END_NONCE!) DO (
:: FOR /L %%G IN (0,3049312,27443808) DO (

ECHO !DATE! !TIME: =0! :: START -- CREATE !NUM_OF_FILES! PLOT-FILES OF !FILE_SIZE!GB ON !DST_PATH!
TIMEOUT 2 /NOBREAK > NUL
ECHO.

FOR /L %%G IN (!START_NONCE!,!NONCES!,!END_NONCE!) DO (
  SET MOVE_DONE=0
  SET CREATE_DONE=0
  
  SET LOOP_NONCE=000000000%%G

  SET /A COUNTER=!COUNTER! + 1
  SET /A MODULO=%%G / !NONCES! %% 2

  IF !MODULO!==0 (
    SET SRC_PATH_CREATE=!SRC_PATH_1!
  ) ELSE (
    SET SRC_PATH_CREATE=!SRC_PATH_2!
  )
  ECHO !DATE! !TIME: =0! :: STEP !COUNTER!/!NUM_OF_FILES! :: START -- CREATE !NUMERIC_ID!_!DISKNUM!!LOOP_NONCE:~-9!_!NONCES! ON !SRC_PATH_CREATE!
  START C:\MINER\engraver-2.4.0-x86_64-pc-windows-msvc\engraver_gpu --path !SRC_PATH_CREATE! --gpu 0:0:8 --id !NUMERIC_ID! --n !NONCES! --mem 12GB --sn !DISKNUM!!LOOP_NONCE:~-9!

  CALL :WaitForCreateAndOrMove
  
  IF !MODULO!==0 (
    SET SRC_PATH_MOVE=!SRC_PATH_1!
  ) ELSE (
    SET SRC_PATH_MOVE=!SRC_PATH_2!
  )
  ECHO !DATE! !TIME: =0! :: STEP !COUNTER!/!NUM_OF_FILES! :: START -- MOVE   !NUMERIC_ID!_!DISKNUM!!LOOP_NONCE:~-9!_!NONCES! TO !DST_PATH!
  START C:\Windows\System32\Robocopy.exe !SRC_PATH_MOVE! !DST_PATH! /R:1 /W:1 /J /NOOFFLOAD /MOV /Z
)

CALL :WaitForCreateAndOrMove
ECHO.
ECHO !DATE! !TIME: =0! :: DONE  -- CREATE !NUM_OF_FILES! PLOT-FILES OF !FILE_SIZE!GB ON !DST_PATH!
GOTO :END

:WaitForCreateAndOrMove
ECHO | SET /P =.
TIMEOUT 30 /NOBREAK > NUL

WMIC PROCESS WHERE "NAME='Robocopy.exe'" GET CommandLine 2>&1 | FIND /I "!SRC_PATH_MOVE!" > NUL
SET RESULT_MOVE=!ERRORLEVEL!
IF !RESULT_MOVE! EQU 1 IF !COUNTER! GTR 1 IF !MOVE_DONE! EQU 0 (
  ECHO !DATE! !TIME: =0! ::           :: DONE  -- MOVE   !NUMERIC_ID!_!DISKNUM!!LOOP_NONCE:~-9!_!NONCES! TO !DST_PATH!
  SET MOVE_DONE=1
)

WMIC PROCESS WHERE "NAME='engraver_gpu.exe'" GET CommandLine 2>&1 | FIND /I "!SRC_PATH_CREATE!" > NUL
SET RESULT_CREATE=!ERRORLEVEL!
IF !RESULT_CREATE! EQU 1 IF !CREATE_DONE! EQU 0 (
  ECHO !DATE! !TIME: =0! ::           :: DONE  -- CREATE !NUMERIC_ID!_!DISKNUM!!LOOP_NONCE:~-9!_!NONCES! ON !SRC_PATH_CREATE!
  SET CREATE_DONE=1
)

SET STATUS=TRUE
IF NOT !RESULT_MOVE! EQU 0 IF NOT !RESULT_CREATE! EQU 0 SET STATUS=FALSE
IF !STATUS!==TRUE (
  GOTO :WaitForCreateAndOrMove
) ELSE (
  ECHO. & GOTO :EOF
)

:END
ENDLOCAL