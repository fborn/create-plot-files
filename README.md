# create-plot-files

This script will create PoC plotfiles using 2 cache disks in parallel to improve plotting time. On my setup it takes roughly 18 hours to plot a 8TB
SMR disk. This script uses engraver_gpu and robocopy

Run:
create-plot-files.bat \<prependnumber> \<numeric-id> \<cache-path-1> \<cache-path-2> \<destination-path> \<nonces>

create-plot-files.bat 2005 <MUMERIC_ID> C:\\_Array1\Disk013\plots C:\\_Array1\Disk014\plots C:\\_Array1\Disk005\plots 3049312

I use 304912 nonces which will leave 1% of space on the disk.

This will create files in the form of: \<NUMERIC-ID>_2005000000000_3049312

WIP:

- using named commandline options
- engraver_gpu options configurable
- making a progress spinnig wheel
- better readable output